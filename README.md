# Resume/CV

A simple resume template made with [Awesome CV][acv].

## Setup

* Install a full latex distribution (`# apt install texlive-full` or whatever command your distribution uses)
* Create a single badge, just use an example.com url for all of it, it will be replaced when you tag and push into gitlab.
* Two variables can be set up. One is optional, the other required for good releases
  * Set up a [Private Access Token][pat] with `api` scope to modify the badges at `$CI_ACCESS_TOKEN`. This should be masked and protected.
  * If you're in a group, you can speed up the builds a tiny bit by using the dependency proxy. Set a `$CI_DEP_PREFIX` variable that is set to `${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/`. The `/` at the end is very important, and it won't work with a user repository, so this is 100% optional.
* Set `v*` as a protected tag so we can use the `$CI_ACCESS_TOKEN` properly.

[acv]: https://github.com/posquit0/Awesome-CV
[pat]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
